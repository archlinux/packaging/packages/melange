# Maintainer: kpcyrd <kpcyrd[at]archlinux[dot]org>

pkgname=melange
pkgver=0.22.1
pkgrel=1
pkgdesc='Build APKs from source code'
url='https://github.com/chainguard-dev/melange'
arch=('x86_64')
license=('Apache-2.0')
depends=(
  'alpine-keyring'
  'apk-tools'
  'bubblewrap'
  'glibc'
)
makedepends=('go')
options=('!lto')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/chainguard-dev/melange/archive/v${pkgver}.tar.gz")
sha256sums=('5493ad4a8a20d18d09471ecadfa4b518d91592c86869c33a64181609a554e236')
b2sums=('012dc5e861863d8c3d7568c70505eb53e25b94d5e4073d0f45843fd1553454a8ff8459a19970cca5fa18032e5b82342cc8766aec7f07c0c6bc47861d89fce72a')

build() {
  cd ${pkgname}-${pkgver}

  export CGO_CPPFLAGS="${CPPFLAGS}"
  export CGO_CFLAGS="${CFLAGS}"
  export CGO_CXXFLAGS="${CXXFLAGS}"
  export CGO_LDFLAGS="${LDFLAGS}"
  export CGO_REQUIRED="1"

  go build \
    -buildmode=pie \
    -mod=readonly \
    -modcacherw \
    -ldflags '-compressdwarf=false -linkmode=external' \
    .
}

package() {
  cd ${pkgname}-${pkgver}
  install -Dm 755 melange -t "${pkgdir}/usr/bin"
  install -dm 755 "${pkgdir}/usr/share/melange"
  cp -a pkg/build/pipelines/ "${pkgdir}/usr/share/melange"
}

# vim: ts=2 sw=2 et:
